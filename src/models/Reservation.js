import {Schema, model} from 'mongoose';

const reservationSchema = new Schema({

    book: { type: Schema.Types.ObjectId, ref: 'Book', required: [true, 'Book reference is required.'] },
    state: { type: String },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: [true, 'User reference is required.'] }
}, {
    timestamps: true,
    versionKey: false 
});
 
export default model('Reservation', reservationSchema);