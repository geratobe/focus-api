import {Schema, model} from 'mongoose';

export const ROLES = ['student',  'librarian'];

const roleSchema = new Schema({
    name: String
}, {
    versionKey: false
});
export default model('Role', roleSchema);