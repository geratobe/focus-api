import {Schema, model} from 'mongoose';

const bookSchema = new Schema({
    title: { type: String, require: [true, 'title is required'] },
    author: { type: String,  require: [true, 'author is required'] },
    published: { type: String,  require: [true, 'published is required'] },
    genre: { type: String, require: [true, 'genre is required'] },
    stock: { type: Number, require: [true, 'stock is required'] },
}, {
    timestamps: true,
    versionKey: false 
})

export default model('Book', bookSchema);