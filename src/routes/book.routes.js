import {Router} from 'express'

const router = Router();

import * as BookCtrl from '../controllers/book.controller'
import {authJwt} from '../middlewares';

router.post('/',[authJwt.verifyToken, authJwt.isLibrarian], BookCtrl.createBook);

router.get('/:id',[authJwt.verifyToken], BookCtrl.getBookById);

router.get('/', BookCtrl.getBooks);

export default router 