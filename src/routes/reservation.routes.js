import {Router} from 'express'

const router = Router();

import * as ReservacionCtrl from '../controllers/reservation.controller';
import {authJwt} from '../middlewares';

router.post('/',[authJwt.verifyToken, authJwt.isStudent], ReservacionCtrl.createReservation);
router.post('/getReservation',[authJwt.verifyToken, authJwt.isStudent], ReservacionCtrl.getReservacionesByStudent);
router.post('/getReservationAll',[authJwt.verifyToken, authJwt.isLibrarian], ReservacionCtrl.getReservaciones);
router.post('/returned',[authJwt.verifyToken, authJwt.isLibrarian], ReservacionCtrl.returnedBook);

export default router 