import {Router} from 'express'

const router = Router()

import * as authCtrl from '../controllers/auth.controller';
import {verifySignup} from '../middlewares';
import { verifyToken } from '../middlewares/authJwt';

router.post('/signup', [verifySignup.checkDuplicatedUserEmail, verifySignup.checkRolesExisted], authCtrl.signUp);
router.post('/signin',  authCtrl.signIn);
router.get('/renew',verifyToken,authCtrl.revalidarToken);



export default router 