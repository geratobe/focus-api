import User from '../models/User';
import jwt from 'jsonwebtoken';
import Role from '../models/Role';
const { generarJWT } = require('../helpers/jwt');
require("dotenv").config();

export const signUp = async (req, res ) => {

    const {name,last_name, email, password, roles} = req.body;

    const newUser = new User({
        name,
        last_name,
        email,
        password: await User.encryptPassword(password)
    });

    if(roles) {
        const foundRole = await Role.find({name: {$in: roles}});
        newUser.roles = foundRole.map(role => role._id);
    }else {
        const role = await Role.findOne({name: 'user'});
        newUser.roles = [role._id];
    }

    const savedUser = await newUser.save();

   const token =  jwt.sign({id: savedUser._id}, process.env.SECRET_JWT_SEED, {
        expiresIn: 86400 // 24 horas
    });

    res.json({
        ok: true,
        id: savedUser.id,
        username: savedUser.username,
        email: savedUser.email,
        roles: savedUser.roles[0].name,
        token:token
    }); 
}

export const signIn = async (req, res ) => {


    const userFound = await User.findOne({email: req.body.email}).populate('roles');

    if(!userFound) {
        return res.status(400).json({ok: false, message: "User not found"});
    }
    const matchPassword = await User.comparePassword(req.body.password, userFound.password);

    if(!matchPassword) {
      return res.status(401).json({ok: false, message: "Invalid password"});
    }

    const token = jwt.sign({uid: userFound._id}, process.env.SECRET_JWT_SEED, {
        expiresIn: '24h' 
    });

    res.json({
        ok: true,
        id: userFound.id,
        username: userFound.username,
        email: userFound.email,
        roles: userFound.roles[0].name,
        token:token
    });
}

export const revalidarToken = async(req, res ) => {

    const { userId } = req;

    //Leer la base de datos

    const dbUser= await User.findById(userId); 


    // Generar el JWT
    const token = await generarJWT( userId, dbUser.username, dbUser.email );

    return res.json({
        ok: true,
        userId, 
        username: dbUser.username,
        email: dbUser.email,
        token
    });

}


