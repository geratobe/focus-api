import Reservation from '../models/Reservation';
import Book from '../models/Book';

export const createReservation = async (req, res) => {

  const body = req.body;
  var newBody = {
    book: body.book,
    user: req.userId,
    state: body.state
  }
  console.log(newBody);

  Reservation.create(newBody).then(async reservationDB => {
    console.log('Hola')

      await reservationDB;

       await Book.updateOne({_id: body.book},{ $set: {'stock': body.stock - 1} } ).then(bookUpdated => {
         console.log(bookUpdated)
       }).catch(err => {
         console.log(err)
       })
       console.log('Hola2')
      res.json({
          ok: true,
          reservation: reservationDB
      });


  }).catch(err => {
      res.json({
        ok: false,
        message: err
      });
  });

}

export const returnedBook = async (req, res) => {
  const {_id, book, state} = req.body;

  if(state === 'returned') {
      res.json({
      ok: false,
      reservation: {}
  });

  }else {
    const reservation = await Reservation.findByIdAndUpdate({_id: _id},{ $set: {'state': 'returned'} }, {new: true});
    await Book.updateOne({_id: book},{ $inc: {'stock': 1} } ).then(bookUpdated => {
    }).catch(err => {
      console.log(err)
    })


  res.json({
      ok: true,
      reservation 
  });

  }

  

}



export const getReservacionesByStudent = async (req, res) => {
  const user  = req.userId

  const reservationes = await Reservation.find({user}).populate('user').populate('book').exec();

  res.json({
      ok: true,
      reservationes 
  });

}
export const getReservaciones = async (req, res) => {

  const reservationes = await Reservation.find().populate('user').populate('book').exec();

  res.json({
      ok: true,
      reservationes 
  });

}








