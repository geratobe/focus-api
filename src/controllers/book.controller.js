import Book from '../models/Book';

export const createBook = async (req, res) => {

  const body = req.body;
  body.usuario = req.userId;


  Book.create(body).then(async bookDB => {

      await bookDB;
      console.log(bookDB)
      res.json({
          ok: true,
          book: bookDB
      })

  }).catch(err => {
      res.json(err);
  });

}

export const getBooks = async (req, res) => {

  let pagina = Number(req.query.pagina) || 1;
  let skip = pagina - 1;
  skip = skip * 10;

  const books = await Book.find().sort({ _id: -1 }).skip(skip).limit(10).exec();

  res.json({
      ok: true,
      pagina,
      books 
  });

}

export const getBookById = async (req, res) => {

  const id = req.params.id;

  console.log(id);

  const book = await Book.findById({_id:id }).exec();

  res.json({
      ok: true,
      book
  });

}



  export const revalidarToken = async(req, res = response ) => {

    const { uid } = req;

    //Leer la base de datos

    const dbUser= await Usuario.findById(uid);

    // Generar el JWT
    const token = await generarJWT( uid, dbUser.name, dbUser.email );

    return res.json({
        ok: true,
        uid, 
        name: dbUser.name,
        email: dbUser.email,
        token
    });

}







