const {uploadToBucket} = require('../helpers/s3');
var numeroDeImagenes = 0;
var imagesUrl = [] ;
const upload = async (req,res) => {
    const bucket = req.body.bucket;
    const file = req.files.file;
 
    console.log(file);
 
    function isObject(val) {
      return val instanceof Array
      } 
 
      if(isObject(file)) {
        imagesUrl = [];
        if(file.length > 5) {
 
          return res.status(400).json({
            ok: false,         mensaje: 'Solo puedes subir un maximo de 5 imagenes'
            });
 
        }
        for (let i = 0; i < file.length; i++) {
             if (!file[i]) {
                 return res.status(400).json({
                     ok: false,
                     mensaje: 'No se subió ningun archivo - image'
                 });
             }
             if (file[i].size >= 1000000) {
                 return res.status(400).json({
                 ok: false,         mensaje: 'El tamaño no debe ser mayor a 1MB'
                 });
             }
             if (!file[i].mimetype.includes('image')) {
             return res.status(400).json({
                 ok: false,
                     mensaje: 'Lo que subió no es una imagen'
                 });
             }
               const result = await uploadToBucket(bucket,file[i]);
 
               imagesUrl.push(result.Location);
            
             
        }
      
      
       }else {
        console.log('Single')
        imagesUrl = [];
        if (!file) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No se subió ningun archivo - image'
            });
        }
        if (file.size >= 1000000) {
            return res.status(400).json({
            ok: false,         mensaje: 'El tamaño no debe ser mayor a 1MB'
            });
        }
        if (!file.mimetype.includes('image')) {
        return res.status(400).json({
            ok: false,
                mensaje: 'Lo que subió no es una imagen'
            });
        }
        numeroDeImagenes = numeroDeImagenes ++;
        if (numeroDeImagenes <= 5) {
            console.log(numeroDeImagenes);
             const result = await uploadToBucket(bucket,file);
             imagesUrl.push(result.Location);
             res.json({ok: true, imagesUrl})
        } else {
            numeroDeImagenes = 0;
            return res.status(400).json({
                ok: false,
                mensaje: 'Solo puedes subir un maximo de 5 imagenes'
            });
        }
       }
 
    //const result = await uploadToBucket(bucket,file);
 
    //res.json(result);
 
  
 
    res.json({
      ok: true,
      imagesUrl: imagesUrl
      });
 
   
};
 
module.exports = {
    upload
}
 
 
