import express from 'express';
import morgan from 'morgan';
import fileUpload from 'express-fileupload';
import pkg from '../package.json';
import {createRoles} from './libs/initialSetup';
import path from 'path';
import bodyParser from 'body-parser';




import authRoute from './routes/auth.routes';
import userRoute from './routes/users.routes';
import bookRoute from './routes/book.routes';
import reservationRoute from './routes/reservation.routes';
import fileRoute from './routes/image-upload-s3.routes';

import cors from 'cors'




const app = express()
createRoles();

const directoryToServer = 'prueba-focus';


app.set('pkg', pkg);

  
app.use(morgan('dev'))
app.use(cors())
app.use(express.json())

// File upload

app.use(fileUpload({useTempFiles: true}));

app.use(bodyParser.urlencoded({limit: '50m'}));
app.use("/", express.static(path.join(__dirname, directoryToServer)));


console.log(path.join(__dirname, directoryToServer));



app.use('/api/auth', authRoute);
app.use('/api/users', userRoute);
app.use('/api/book', bookRoute);
app.use('/api/reservation', reservationRoute);
app.use('/api/file-upload', fileRoute);

export default app;   