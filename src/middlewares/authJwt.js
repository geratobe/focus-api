import jwt from 'jsonwebtoken';
import Role from '../models/Role';
import User from '../models/User';
export const verifyToken = async (req, res, next) => {
    
try {
    const token = req.headers['x-access-token'];
    console.log(token);

    if(!token) {
        return res.status(403).json({message: 'No Token provider'});
    }
    const decoded = jwt.verify(token, process.env.SECRET_JWT_SEED);
    console.log('decoded',decoded)
    req.userId = decoded.uid;
    console.log(req.userId)
    const user = await User.findById(req.userId, {password: 0});
    if(!user) {
        return res.status(404).json({message: 'no user found'});
    }
    next();
} catch (error) {
    console.log(error);
    return res.status(500).json({message: 'Unautorized'});
}

}

export const isStudent = async (req, res, next) => {
    const user = await User.findById(req.userId);
    const roles = await Role.find({_id: {$in: user.roles}});
    for (let index = 0; index < roles.length; index++) {
        if(roles[index].name === 'student'){
            next();
            return;
        }
    }
    return res.status(403).json({message: 'Require student Role'})

}

export const isLibrarian = async (req, res, next) => {
    const user = await User.findById(req.userId);
    const roles = await Role.find({_id: {$in: user.roles}});
    for (let index = 0; index < roles.length; index++) {
        if(roles[index].name === 'librarian'){
            next();
            return;
        }
    }
    return res.status(403).json({message: 'Require librarian Role'})

}



export const validarJWT = ( req, res = response, next ) => {

    const token = req.header('x-access-token');

    if( !token  ) {
        return res.status(401).json({
            ok: false,
            msg: 'error en el token'
        });
    }

    try {

        const { uid, username } = jwt.verify( token, process.env.SECRET_JWT_SEED );
        req.uid  = uid;
        req.username = username;

        
    } catch (error) {
        return res.status(401).json({
            ok: false,
            msg: 'Token no válido'
        });
    }



    // TODO OK!
    next();
}