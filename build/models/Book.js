"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var bookSchema = new _mongoose.Schema({
  title: {
    type: String,
    require: [true, 'title is required']
  },
  author: {
    type: String,
    require: [true, 'author is required']
  },
  published: {
    type: String,
    require: [true, 'published is required']
  },
  genre: {
    type: String,
    require: [true, 'genre is required']
  },
  stock: {
    type: Number,
    require: [true, 'stock is required']
  }
}, {
  timestamps: true,
  versionKey: false
});

var _default = (0, _mongoose.model)('Book', bookSchema);

exports["default"] = _default;