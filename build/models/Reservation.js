"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var reservationSchema = new _mongoose.Schema({
  book: {
    type: _mongoose.Schema.Types.ObjectId,
    ref: 'Book',
    required: [true, 'Book reference is required.']
  },
  state: {
    type: String
  },
  user: {
    type: _mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User reference is required.']
  }
}, {
  timestamps: true,
  versionKey: false
});

var _default = (0, _mongoose.model)('Reservation', reservationSchema);

exports["default"] = _default;