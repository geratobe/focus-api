"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _morgan = _interopRequireDefault(require("morgan"));

var _expressFileupload = _interopRequireDefault(require("express-fileupload"));

var _package = _interopRequireDefault(require("./package.json"));

var _initialSetup = require("./libs/initialSetup");

var _path = _interopRequireDefault(require("path"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _auth = _interopRequireDefault(require("./routes/auth.routes"));

var _users = _interopRequireDefault(require("./routes/users.routes"));

var _book = _interopRequireDefault(require("./routes/book.routes"));

var _reservation = _interopRequireDefault(require("./routes/reservation.routes"));

var _imageUploadS = _interopRequireDefault(require("./routes/image-upload-s3.routes"));

var _cors = _interopRequireDefault(require("cors"));

var app = (0, _express["default"])();
(0, _initialSetup.createRoles)();
var directoryToServer = 'prueba-focus';
app.set('pkg', _package["default"]);
app.use((0, _morgan["default"])('dev'));
app.use((0, _cors["default"])());
app.use(_express["default"].json()); // File upload

app.use((0, _expressFileupload["default"])({
  useTempFiles: true
}));
app.use(_bodyParser["default"].urlencoded({
  limit: '50m'
}));
app.use("/", _express["default"]["static"](_path["default"].join(__dirname, directoryToServer)));
console.log(_path["default"].join(__dirname, directoryToServer));
app.use('/api/auth', _auth["default"]);
app.use('/api/users', _users["default"]);
app.use('/api/book', _book["default"]);
app.use('/api/reservation', _reservation["default"]);
app.use('/api/file-upload', _imageUploadS["default"]);
var _default = app;
exports["default"] = _default;