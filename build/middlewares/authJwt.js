"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validarJWT = exports.isLibrarian = exports.isStudent = exports.verifyToken = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _Role = _interopRequireDefault(require("../models/Role"));

var _User = _interopRequireDefault(require("../models/User"));

var verifyToken = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res, next) {
    var token, decoded, user;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            token = req.headers['x-access-token'];
            console.log(token);

            if (token) {
              _context.next = 5;
              break;
            }

            return _context.abrupt("return", res.status(403).json({
              message: 'No Token provider'
            }));

          case 5:
            decoded = _jsonwebtoken["default"].verify(token, process.env.SECRET_JWT_SEED);
            console.log('decoded', decoded);
            req.userId = decoded.uid;
            console.log(req.userId);
            _context.next = 11;
            return _User["default"].findById(req.userId, {
              password: 0
            });

          case 11:
            user = _context.sent;

            if (user) {
              _context.next = 14;
              break;
            }

            return _context.abrupt("return", res.status(404).json({
              message: 'no user found'
            }));

          case 14:
            next();
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](0);
            console.log(_context.t0);
            return _context.abrupt("return", res.status(500).json({
              message: 'Unautorized'
            }));

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 17]]);
  }));

  return function verifyToken(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.verifyToken = verifyToken;

var isStudent = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res, next) {
    var user, roles, index;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _User["default"].findById(req.userId);

          case 2:
            user = _context2.sent;
            _context2.next = 5;
            return _Role["default"].find({
              _id: {
                $in: user.roles
              }
            });

          case 5:
            roles = _context2.sent;
            index = 0;

          case 7:
            if (!(index < roles.length)) {
              _context2.next = 14;
              break;
            }

            if (!(roles[index].name === 'student')) {
              _context2.next = 11;
              break;
            }

            next();
            return _context2.abrupt("return");

          case 11:
            index++;
            _context2.next = 7;
            break;

          case 14:
            return _context2.abrupt("return", res.status(403).json({
              message: 'Require student Role'
            }));

          case 15:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function isStudent(_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

exports.isStudent = isStudent;

var isLibrarian = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res, next) {
    var user, roles, index;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return _User["default"].findById(req.userId);

          case 2:
            user = _context3.sent;
            _context3.next = 5;
            return _Role["default"].find({
              _id: {
                $in: user.roles
              }
            });

          case 5:
            roles = _context3.sent;
            index = 0;

          case 7:
            if (!(index < roles.length)) {
              _context3.next = 14;
              break;
            }

            if (!(roles[index].name === 'librarian')) {
              _context3.next = 11;
              break;
            }

            next();
            return _context3.abrupt("return");

          case 11:
            index++;
            _context3.next = 7;
            break;

          case 14:
            return _context3.abrupt("return", res.status(403).json({
              message: 'Require librarian Role'
            }));

          case 15:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function isLibrarian(_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
}();

exports.isLibrarian = isLibrarian;

var validarJWT = function validarJWT(req) {
  var res = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : response;
  var next = arguments.length > 2 ? arguments[2] : undefined;
  var token = req.header('x-access-token');

  if (!token) {
    return res.status(401).json({
      ok: false,
      msg: 'error en el token'
    });
  }

  try {
    var _jwt$verify = _jsonwebtoken["default"].verify(token, process.env.SECRET_JWT_SEED),
        uid = _jwt$verify.uid,
        username = _jwt$verify.username;

    req.uid = uid;
    req.username = username;
  } catch (error) {
    return res.status(401).json({
      ok: false,
      msg: 'Token no válido'
    });
  } // TODO OK!


  next();
};

exports.validarJWT = validarJWT;