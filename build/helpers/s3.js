"use strict";

var S3 = require('aws-sdk/clients/s3');

var fs = require('fs');

var region = 'us-east-2';
var accessKeyId = 'AKIA5QXFOTTTU65BJ4EJ';
var secretAccessKey = '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';
var storage = new S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

var getBuckets = function getBuckets() {
  return storage.listBuckets().promise();
};

var uploadToBucket = function uploadToBucket(bucketName, file) {
  var stream = fs.createReadStream(file.tempFilePath);
  var params = {
    Bucket: bucketName,
    Key: file.name,
    Body: stream
  };
  return storage.upload(params).promise();
};

module.exports = {
  getBuckets: getBuckets,
  uploadToBucket: uploadToBucket
};