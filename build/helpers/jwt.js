"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generarJWT = void 0;

var jwt = require('jsonwebtoken');

require("dotenv").config();

var generarJWT = function generarJWT(uid, name) {
  var payload = {
    uid: uid,
    name: name
  };
  return new Promise(function (resolve, reject) {
    jwt.sign(payload, process.env.SECRET_JWT_SEED, {
      expiresIn: 31536000
    }, function (err, token) {
      if (err) {
        // TODO MAL
        console.log(err);
        reject(err);
      } else {
        // TODO BIEN
        resolve(token);
      }
    });
  });
};

exports.generarJWT = generarJWT;