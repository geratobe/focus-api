"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _app = _interopRequireDefault(require("./app"));

var _database = _interopRequireDefault(require("./database"));

_app["default"].listen(process.env.PORT || 4001);

console.log('Server on port', 4001);