"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _require = require('../helpers/s3'),
    uploadToBucket = _require.uploadToBucket;

var numeroDeImagenes = 0;
var imagesUrl = [];

var upload = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var bucket, file, isObject, i, result, _result;

    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            isObject = function _isObject(val) {
              return val instanceof Array;
            };

            bucket = req.body.bucket;
            file = req.files.file;
            console.log(file);

            if (!isObject(file)) {
              _context.next = 25;
              break;
            }

            imagesUrl = [];

            if (!(file.length > 5)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'Solo puedes subir un maximo de 5 imagenes'
            }));

          case 8:
            i = 0;

          case 9:
            if (!(i < file.length)) {
              _context.next = 23;
              break;
            }

            if (file[i]) {
              _context.next = 12;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'No se subió ningun archivo - image'
            }));

          case 12:
            if (!(file[i].size >= 1000000)) {
              _context.next = 14;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'El tamaño no debe ser mayor a 1MB'
            }));

          case 14:
            if (file[i].mimetype.includes('image')) {
              _context.next = 16;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'Lo que subió no es una imagen'
            }));

          case 16:
            _context.next = 18;
            return uploadToBucket(bucket, file[i]);

          case 18:
            result = _context.sent;
            imagesUrl.push(result.Location);

          case 20:
            i++;
            _context.next = 9;
            break;

          case 23:
            _context.next = 45;
            break;

          case 25:
            console.log('Single');
            imagesUrl = [];

            if (file) {
              _context.next = 29;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'No se subió ningun archivo - image'
            }));

          case 29:
            if (!(file.size >= 1000000)) {
              _context.next = 31;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'El tamaño no debe ser mayor a 1MB'
            }));

          case 31:
            if (file.mimetype.includes('image')) {
              _context.next = 33;
              break;
            }

            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'Lo que subió no es una imagen'
            }));

          case 33:
            numeroDeImagenes = numeroDeImagenes++;

            if (!(numeroDeImagenes <= 5)) {
              _context.next = 43;
              break;
            }

            console.log(numeroDeImagenes);
            _context.next = 38;
            return uploadToBucket(bucket, file);

          case 38:
            _result = _context.sent;
            imagesUrl.push(_result.Location);
            res.json({
              ok: true,
              imagesUrl: imagesUrl
            });
            _context.next = 45;
            break;

          case 43:
            numeroDeImagenes = 0;
            return _context.abrupt("return", res.status(400).json({
              ok: false,
              mensaje: 'Solo puedes subir un maximo de 5 imagenes'
            }));

          case 45:
            //const result = await uploadToBucket(bucket,file);
            //res.json(result);
            res.json({
              ok: true,
              imagesUrl: imagesUrl
            });

          case 46:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function upload(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

module.exports = {
  upload: upload
};