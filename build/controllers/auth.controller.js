"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.revalidarToken = exports.signIn = exports.signUp = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _User = _interopRequireDefault(require("../models/User"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _Role = _interopRequireDefault(require("../models/Role"));

var _require = require('../helpers/jwt'),
    generarJWT = _require.generarJWT;

require("dotenv").config();

var signUp = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var _req$body, name, last_name, email, password, roles, newUser, foundRole, role, savedUser, token;

    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$body = req.body, name = _req$body.name, last_name = _req$body.last_name, email = _req$body.email, password = _req$body.password, roles = _req$body.roles;
            _context.t0 = _User["default"];
            _context.t1 = name;
            _context.t2 = last_name;
            _context.t3 = email;
            _context.next = 7;
            return _User["default"].encryptPassword(password);

          case 7:
            _context.t4 = _context.sent;
            _context.t5 = {
              name: _context.t1,
              last_name: _context.t2,
              email: _context.t3,
              password: _context.t4
            };
            newUser = new _context.t0(_context.t5);

            if (!roles) {
              _context.next = 17;
              break;
            }

            _context.next = 13;
            return _Role["default"].find({
              name: {
                $in: roles
              }
            });

          case 13:
            foundRole = _context.sent;
            newUser.roles = foundRole.map(function (role) {
              return role._id;
            });
            _context.next = 21;
            break;

          case 17:
            _context.next = 19;
            return _Role["default"].findOne({
              name: 'user'
            });

          case 19:
            role = _context.sent;
            newUser.roles = [role._id];

          case 21:
            _context.next = 23;
            return newUser.save();

          case 23:
            savedUser = _context.sent;
            token = _jsonwebtoken["default"].sign({
              id: savedUser._id
            }, process.env.SECRET_JWT_SEED, {
              expiresIn: 86400 // 24 horas

            });
            res.json({
              ok: true,
              id: savedUser.id,
              username: savedUser.username,
              email: savedUser.email,
              roles: savedUser.roles[0].name,
              token: token
            });

          case 26:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function signUp(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.signUp = signUp;

var signIn = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var userFound, matchPassword, token;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return _User["default"].findOne({
              email: req.body.email
            }).populate('roles');

          case 2:
            userFound = _context2.sent;

            if (userFound) {
              _context2.next = 5;
              break;
            }

            return _context2.abrupt("return", res.status(400).json({
              ok: false,
              message: "User not found"
            }));

          case 5:
            _context2.next = 7;
            return _User["default"].comparePassword(req.body.password, userFound.password);

          case 7:
            matchPassword = _context2.sent;

            if (matchPassword) {
              _context2.next = 10;
              break;
            }

            return _context2.abrupt("return", res.status(401).json({
              ok: false,
              message: "Invalid password"
            }));

          case 10:
            token = _jsonwebtoken["default"].sign({
              uid: userFound._id
            }, process.env.SECRET_JWT_SEED, {
              expiresIn: '24h'
            });
            res.json({
              ok: true,
              id: userFound.id,
              username: userFound.username,
              email: userFound.email,
              roles: userFound.roles[0].name,
              token: token
            });

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function signIn(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.signIn = signIn;

var revalidarToken = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var userId, dbUser, token;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            userId = req.userId; //Leer la base de datos

            _context3.next = 3;
            return _User["default"].findById(userId);

          case 3:
            dbUser = _context3.sent;
            _context3.next = 6;
            return generarJWT(userId, dbUser.username, dbUser.email);

          case 6:
            token = _context3.sent;
            return _context3.abrupt("return", res.json({
              ok: true,
              userId: userId,
              username: dbUser.username,
              email: dbUser.email,
              token: token
            }));

          case 8:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function revalidarToken(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

exports.revalidarToken = revalidarToken;