"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getReservaciones = exports.getReservacionesByStudent = exports.returnedBook = exports.createReservation = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _Reservation = _interopRequireDefault(require("../models/Reservation"));

var _Book = _interopRequireDefault(require("../models/Book"));

var createReservation = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var body, newBody;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            body = req.body;
            newBody = {
              book: body.book,
              user: req.userId,
              state: body.state
            };
            console.log(newBody);

            _Reservation["default"].create(newBody).then( /*#__PURE__*/function () {
              var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(reservationDB) {
                return _regenerator["default"].wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        console.log('Hola');
                        _context.next = 3;
                        return reservationDB;

                      case 3:
                        _context.next = 5;
                        return _Book["default"].updateOne({
                          _id: body.book
                        }, {
                          $set: {
                            'stock': body.stock - 1
                          }
                        }).then(function (bookUpdated) {
                          console.log(bookUpdated);
                        })["catch"](function (err) {
                          console.log(err);
                        });

                      case 5:
                        console.log('Hola2');
                        res.json({
                          ok: true,
                          reservation: reservationDB
                        });

                      case 7:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x3) {
                return _ref2.apply(this, arguments);
              };
            }())["catch"](function (err) {
              res.json({
                ok: false,
                message: err
              });
            });

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function createReservation(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.createReservation = createReservation;

var returnedBook = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var _req$body, _id, book, state, reservation;

    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _req$body = req.body, _id = _req$body._id, book = _req$body.book, state = _req$body.state;

            if (!(state === 'returned')) {
              _context3.next = 5;
              break;
            }

            res.json({
              ok: false,
              reservation: {}
            });
            _context3.next = 12;
            break;

          case 5:
            _context3.next = 7;
            return _Reservation["default"].findByIdAndUpdate({
              _id: _id
            }, {
              $set: {
                'state': 'returned'
              }
            }, {
              "new": true
            });

          case 7:
            reservation = _context3.sent;
            console.log(reservation);
            _context3.next = 11;
            return _Book["default"].updateOne({
              _id: book
            }, {
              $inc: {
                'stock': 1
              }
            }).then(function (bookUpdated) {
              console.log(bookUpdated);
            })["catch"](function (err) {
              console.log(err);
            });

          case 11:
            res.json({
              ok: true,
              reservation: reservation
            });

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function returnedBook(_x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.returnedBook = returnedBook;

var getReservacionesByStudent = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var user, reservationes;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            user = req.userId;
            _context4.next = 3;
            return _Reservation["default"].find({
              user: user
            }).populate('user').populate('book').exec();

          case 3:
            reservationes = _context4.sent;
            res.json({
              ok: true,
              reservationes: reservationes
            });

          case 5:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getReservacionesByStudent(_x6, _x7) {
    return _ref4.apply(this, arguments);
  };
}();

exports.getReservacionesByStudent = getReservacionesByStudent;

var getReservaciones = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
    var reservationes;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return _Reservation["default"].find().populate('user').populate('book').exec();

          case 2:
            reservationes = _context5.sent;
            res.json({
              ok: true,
              reservationes: reservationes
            });

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function getReservaciones(_x8, _x9) {
    return _ref5.apply(this, arguments);
  };
}();

exports.getReservaciones = getReservaciones;