"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.revalidarToken = exports.getBookById = exports.getBooks = exports.createBook = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _Book = _interopRequireDefault(require("../models/Book"));

var createBook = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var body;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            body = req.body;
            body.usuario = req.userId;

            _Book["default"].create(body).then( /*#__PURE__*/function () {
              var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(bookDB) {
                return _regenerator["default"].wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return bookDB;

                      case 2:
                        console.log(bookDB);
                        res.json({
                          ok: true,
                          book: bookDB
                        });

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x3) {
                return _ref2.apply(this, arguments);
              };
            }())["catch"](function (err) {
              res.json(err);
            });

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function createBook(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.createBook = createBook;

var getBooks = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var pagina, skip, books;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            pagina = Number(req.query.pagina) || 1;
            skip = pagina - 1;
            skip = skip * 10;
            _context3.next = 5;
            return _Book["default"].find().sort({
              _id: -1
            }).skip(skip).limit(10).exec();

          case 5:
            books = _context3.sent;
            res.json({
              ok: true,
              pagina: pagina,
              books: books
            });

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function getBooks(_x4, _x5) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getBooks = getBooks;

var getBookById = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var id, book;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            id = req.params.id;
            console.log(id);
            _context4.next = 4;
            return _Book["default"].findById({
              _id: id
            }).exec();

          case 4:
            book = _context4.sent;
            res.json({
              ok: true,
              book: book
            });

          case 6:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getBookById(_x6, _x7) {
    return _ref4.apply(this, arguments);
  };
}();

exports.getBookById = getBookById;

var revalidarToken = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req) {
    var res,
        uid,
        dbUser,
        token,
        _args5 = arguments;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            res = _args5.length > 1 && _args5[1] !== undefined ? _args5[1] : response;
            uid = req.uid; //Leer la base de datos

            _context5.next = 4;
            return Usuario.findById(uid);

          case 4:
            dbUser = _context5.sent;
            _context5.next = 7;
            return generarJWT(uid, dbUser.name, dbUser.email);

          case 7:
            token = _context5.sent;
            return _context5.abrupt("return", res.json({
              ok: true,
              uid: uid,
              name: dbUser.name,
              email: dbUser.email,
              token: token
            }));

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function revalidarToken(_x8) {
    return _ref5.apply(this, arguments);
  };
}();

exports.revalidarToken = revalidarToken;