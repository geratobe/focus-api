"use strict";

var _require = require('express'),
    Router = _require.Router;

var _require2 = require('../controllers/upload.controller'),
    upload = _require2.upload;

var _require3 = require('../middlewares/verifyFile'),
    verifyFile = _require3.verifyFile;

var router = Router();
router.post('/', verifyFile, upload);
module.exports = router;